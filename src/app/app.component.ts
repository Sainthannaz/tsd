import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Platform, Config, AlertController, LoadingController, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Storage } from '@ionic/storage';
import { Router } from  '@angular/router';
import { Keyboard } from '@ionic-native/keyboard/ngx';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  alertText: any = {};
  loader: any;
  lang: boolean;
  public appPages = [
    {
      title: 'MENU_DASHBOARD',
      url: '/dashboard',
      icon: 'home'
    },    
    {
      title: 'MENU_ABOUT',
      url: '/about',
      icon: 'alert'
    }
  ];
  
  userData: { uid: string, email: string } = {
    uid: '',
    email: ''
  };


  constructor(
    private platform: Platform, public toastCtrl: ToastController, private screenOrientation: ScreenOrientation,
    private splashScreen: SplashScreen, private config: Config, public loadingCtrl: LoadingController, 
    private statusBar: StatusBar, private translate: TranslateService, private storage: Storage, private router: Router, public alertCtrl: AlertController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
    this.statusBar.styleDefault();
    this.statusBar.overlaysWebView(false);
    this.statusBar.backgroundColorByHexString('#005CA9');

    this.splashScreen.hide();
    });
    this.initTranslate();
    // orientacion actual
    console.log(this.screenOrientation.type); 

    // fijamos a tipo retrato
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);

    this.translate.get('ALERT_TITLE_EXIT').subscribe(title => {
      this.alertText.title = title;
    });

    this.translate.get('ALERT_MESSAGE_EXIT').subscribe(message => {
      this.alertText.message = message;
    });

    this.translate.get('ALERT_OK').subscribe(ok => {
      this.alertText.ok = ok;
    });

    this.translate.get('ALERT_CANCEL').subscribe(cancel => {
      this.alertText.cancel = cancel;
    });
  }

  

  initTranslate() {    
   let language = this.translate.getBrowserLang();
   this.translate.setDefaultLang(language);
   this.translate.use(language);
   console.log(language);
   if (language == 'es') {
     this.lang = true;
     console.log("es español");
   } else {
     this.lang = false;
     console.log("es ingles");
   }   
    //this.translate.setDefaultLang('es');
    //const browserLang = this.translate.getBrowserLang();    
    //this.translate.use('es'); // Set your language here
    
    // Muestra el texto Atras en la toolbar
    /*this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('backButtonText', values.BACK_BUTTON_TEXT);
    });*/     
  }
  
  myChange($event){
    this.lang = !this.lang;
    console.log(this.lang);
    if (this.lang == true) {
      console.log('1');
      this.translate.use('en') 
    } else if (this.lang == false) {
      console.log('2');
     this.translate.use('es');
    }
  }

  closeSession(){
     this.presentAlert();       
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      message: 'Auth...',
      spinner: 'crescent',
      duration: 2000
    });
    this.loader.present();
  }

  async showLongToast() {
    const toast = await this.toastCtrl.create({
      message: '...',
      duration: 4000,
      position: 'bottom'
    });
    toast.present();
  }

  async presentAlert() {
    console.log('Alert Event');
    const alert = await this.alertCtrl.create({
      header: this.alertText.title,    
      message: this.alertText.message,
      buttons: [ {
          text: this.alertText.cancel,
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text:  this.alertText.ok,
          handler: () => {
            console.log('Confirm Ok');
             this.storage.set('loginSession', 'false');
             this.router.navigateByUrl('inicio');    
          }
        }]
    });

    await alert.present();
  }

}
