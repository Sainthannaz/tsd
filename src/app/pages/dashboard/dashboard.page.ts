import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MenuController, NavController, Platform, ToastController, LoadingController, AlertController } from '@ionic/angular';
import { Router } from  "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { CartService } from '../../services/cart.service';
import { Plasticos } from "../../mocks/providers/plasticos";
import { Plastico } from "../../models/plastico";
import { Procesos } from "../../mocks/providers/procesos";
import { Proceso } from "../../models/proceso";
import { Problems } from "../../mocks/providers/problems";
import { Problem } from "../../models/problem";


import { AuthenticationService } from './../../services/authentication.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})


export class DashboardPage implements OnInit {

  @ViewChild('slider', { read: ElementRef })slider: ElementRef;
  img: any;
  
  inspeccion_procesos:any; 
  problemas_procesos:any;
  currentPolietileno: any = [];
  currentPolipropileno: any = [];
  public polietileno:boolean = false;
  public polipropileno:boolean = false;

  sliderOpts = {
    autoplay: true,
    zoom: {
      maxRatio: 5
    }
  };
  
  alertText: any = {};
  constructor(public navCtrl: NavController,  public platform: Platform, public loadingCtrl: LoadingController, 
   public menu: MenuController, private translate: TranslateService, private storage: Storage, private router: Router, 
   public alertCtrl: AlertController, 
   public plasticos: Plasticos, public procesos: Procesos, public problemas: Problems, 
   private authenticationService: AuthenticationService) {
    this.platform.backButton.subscribeWithPriority(0, () => {
     this.presentAlert();    
    });

  }

  ngOnInit() {
    this.translate.get('ALERT_TITLE_EXIT').subscribe(title => {
      this.alertText.title = title;
    });
    this.translate.get('ALERT_MESSAGE_EXIT').subscribe(message => {
      this.alertText.message = message;
    });
    this.translate.get('ALERT_OK').subscribe(ok => {
      this.alertText.ok = ok;
    });
    this.translate.get('ALERT_CANCEL').subscribe(cancel => {
      this.alertText.cancel = cancel;
    });
    this.translate.get('CART_ITEM_ADDED').subscribe(item => {
        this.alertText.item = item;
    });   
    this.loadDataPlasticos();
    //this.loadDataProcesos();
    //this.loadDataCausa();
    //this.loadDataSolucion();

    this.currentPolietileno = this.procesos.query();    
    this.storage.set('loginSession', 'true');
    
  }

  loadDataPlasticos(){
    /* CARGAMOS LOS PLASTICOS INICIALES */
   this.procesos.deleteAll();
   this.inspeccion_procesos = ({"id":1, "plastico":1, "name": 'MOLDEADO', "image":'https://picsum.photos/400/200'});  
   this.procesos.add(this.inspeccion_procesos);
   this.inspeccion_procesos = ({"id":2, "plastico":1, "name": 'EXTRUSION', "image":'https://picsum.photos/400/200'});  
   this.procesos.add(this.inspeccion_procesos);
   this.inspeccion_procesos = ({"id":3, "plastico":1, "name": 'MONOFILAMENTOS', "image":'https://picsum.photos/400/200'});  
   this.procesos.add(this.inspeccion_procesos);
   this.inspeccion_procesos = ({"id":4, "plastico":1, "name": 'TERMOFORMADO', "image":'https://picsum.photos/400/200'});  
   this.procesos.add(this.inspeccion_procesos);
   this.inspeccion_procesos = ({"id":5, "plastico":1, "name": 'ETXTUBOS', "image":'https://picsum.photos/400/200'});  
   this.procesos.add(this.inspeccion_procesos);   
   this.inspeccion_procesos = ({"id":6, "plastico":1, "name": 'EXTPEL', "image":'https://picsum.photos/400/200'});  
   this.procesos.add(this.inspeccion_procesos);   
   this.inspeccion_procesos = ({"id":7, "plastico":1, "name": 'COEXTPEL', "image":'https://picsum.photos/400/200'});  
   this.procesos.add(this.inspeccion_procesos);   

   console.log(this.procesos);
  }

  /*
  loadDataCausa(){
    this.problemas_procesos = ({ "problema": 1,  "causa": 1, "text":'PROBLEM_1' });
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "problema": 1,  "causa": 2, "text":'PROBLEM_2' });
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "problema": 1,  "causa": 3, "text":'PROBLEM_3' });
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "problema": 2,  "causa": 1, "text":'PROBLEM_4' });
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "problema": 2,  "causa": 2, "text":'PROBLEM_5' });
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "problema": 2,  "causa": 3, "text":'PROBLEM_6' });
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "problema": 2,  "causa": 4, "text":'PROBLEM_7' });
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "problema": 3,  "causa": 1, "text":'PROBLEM_8' });
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "problema": 3,  "causa": 2, "text":'PROBLEM_9' });
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "problema": 3,  "causa": 3, "text":'PROBLEM_10' });
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "problema": 4,  "causa": 1, "text":'PROBLEM_11' });
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "problema": 5,  "causa": 1, "text":'PROBLEM_12' });
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "problema": 5,  "causa": 2, "text":'PROBLEM_13' });
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "problema": 5,  "causa": 3, "text":'PROBLEM_14' });
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "problema": 5,  "causa": 4, "text":'PROBLEM_15' });
    this.problemas.add(this.problemas_procesos);
  }

  loadDataSolucion(){
    this.problemas_procesos = ({ "causa": 1,  "solucion": 1, "text":'' });
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "causa": 1,  "solucion": 1, "text":'' });
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "causa": 1,  "solucion": 1, "text":'' });
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "causa": 1,  "solucion": 1, "text":'' });
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "causa": 1,  "solucion": 1, "text":'' });
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "causa": 1,  "solucion": 1, "text":'' });
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "causa": 1,  "solucion": 1, "text":'' });
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "causa": 1,  "solucion": 1, "text":'' });
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "causa": 1,  "solucion": 1, "text":'' });
    this.problemas.add(this.problemas_procesos);

  }

  */
  ionViewDidLoad() {
     
  }

  ionViewDidEnter() {    
    this.authenticationService.authenticationState.subscribe(state => {
      if (state) {
        console.log(state);
        this.router.navigateByUrl('dashboard');
      } else {
        console.log(state);
        this.router.navigateByUrl('home');
      }
    });  
    this.menu.enable(true);    
   
  }

 about(){
    this.router.navigateByUrl('about');
 }

 togglePolietileno(){
   this.polietileno = !this.polietileno;
 }

 togglePolipropileno(){
   this.polipropileno = !this.polipropileno;
 }

 openItem(detail){
   console.log(detail);
   console.log('Intentar abrir');
   this.router.navigate(['/detail', detail]);
 }


 async presentAlert() {
    console.log('Alert Event');
    const alert = await this.alertCtrl.create({
      header: this.alertText.title,  
      cssClass: 'exitAlert',  
      message: this.alertText.message,     
      buttons: [ {
          text: this.alertText.cancel,
          role: 'cancel',
          cssClass: 'exitAlert',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text:  this.alertText.ok,
          cssClass: 'exitAlert',
          handler: () => {
            console.log('Confirm Ok');
             this.storage.set('loginSession', 'false');
             this.authenticationService.logout();
             this.router.navigateByUrl('home');    
          }
        }]
    });

    await alert.present();
  }
}
