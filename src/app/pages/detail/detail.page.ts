import { Component, OnInit, ViewChild, ElementRef, Directive, Input, Renderer2, SimpleChanges} from '@angular/core';
import { MenuController, NavController, Platform, ToastController, LoadingController, IonSlides, DomController, AlertController  } from '@ionic/angular';
import {ActivatedRoute} from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ScrollDetail,  } from '@ionic/core';
import { Router } from  "@angular/router";
import { Problems } from "../../mocks/providers/problems";
import { Problem } from "../../models/problem";
import { CartService, Item } from '../../services/cart.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
  @ViewChild(IonSlides) slides: IonSlides;
  showToolbar = false;
  value : any;  
  today: any ;
  personalizar: boolean = false;
  filterProblemas: Problem[];
  problemas_procesos:any;
  currentProblemas: any = [];  
  detail: any = [];
  
  items: Item[] = [];
  newItem: Item = <Item>{};
  next: boolean;
  cart: boolean;
  problems: any = [];
  alertText: any = {};

  constructor(public navCtrl: NavController,  private route: ActivatedRoute, public platform: Platform, public loadingCtrl: LoadingController, private translate: TranslateService,
    public menu: MenuController, private  router:  Router,  public toastCtrl: ToastController, public alertCtrl: AlertController,  public problemas: Problems) {    
    this.detail.id = this.route.snapshot.paramMap.get('id');    
    this.detail.name = this.route.snapshot.paramMap.get('name');
    this.detail.image = this.route.snapshot.paramMap.get('image');
    this.filterProblemas = this.problemas.query();
    //this.problemas.add(this.filterProblemas);
    this.problems = this.filterProblemas;

  }

  ngOnInit() {
    this.translate.get('CART_TITLE_ADD').subscribe(title => {
      this.alertText.title = title;
    });

    this.translate.get('CART_MESSAGE_DETAIL').subscribe(message => {
      this.alertText.message = message;
    });

    this.translate.get('ALERT_OK').subscribe(ok => {
      this.alertText.ok = ok;
    });

    this.translate.get('ALERT_CANCEL').subscribe(cancel => {
      this.alertText.cancel = cancel;
    });    
    this.translate.get('CART_ITEM_ADDED').subscribe(item => {
        this.alertText.item = item;
    });
    this.loadDataProcesos();
    //this.problems = this.problemas.query();    
  
  }

   loadDataProcesos(){
    this.problemas.deleteAll();

    /* MOLDEO POR INYECCIÓN */

    this.problemas_procesos = ({ "id":1, "plastico": 1, "problema": 1, "proceso":1 , "text":'INJECTION_1', "tags":'Contracción muy alta Decrease the dough temperature'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":2, "plastico": 1, "problema": 2, "proceso":1 , "text":'INJECTION_2', "tags":' Deformación Deformation'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":3, "plastico": 1, "problema": 3, "proceso":1 ,  "text":'INJECTION_3', "tags":'Poco brillo Low brightness'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":4, "plastico": 1, "problema": 4, "proceso":1 ,  "text":'INJECTION_4', "tags":'Puntos negros Black spots'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":5, "plastico": 1, "problema": 5, "proceso":1 ,  "text":'INJECTION_5', "tags":'Rayas negras Black stripes'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":6, "plastico": 1, "problema": 6, "proceso":1 ,  "text":'INJECTION_6', "tags":'Fragilidad Fragility'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":7, "plastico": 1, "problema": 7, "proceso":1 ,  "text":'INJECTION_7', "tags":'Burbujas Bubbles'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":8, "plastico": 1, "problema": 8, "proceso":1 ,  "text":'INJECTION_8', "tags":'Puntos claros Clear points'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":9, "plastico": 1, "problema": 9, "proceso":1 ,  "text":'INJECTION_9', "tags":'Delaminación Delamination'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":10, "plastico": 1, "problema": 10, "proceso":1 ,  "text":'INJECTION_10', "tags":'Dificultad de extracción de la pieza del molde Difficulty removing the mold part'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":11, "plastico": 1, "problema": 11, "proceso":1 ,  "text":'INJECTION_11', "tags":'Acabado superficial deficiente en la pieza Poor surface finish on the part'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":12, "plastico": 1, "problema": 12, "proceso":1 ,  "text":'INJECTION_12', "tags":'Coloración deficiente en la pieza Poor color in the piece'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":13, "plastico": 1, "problema": 13, "proceso":1 ,  "text":'INJECTION_13', "tags":'Rebaba Burr'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":14, "plastico": 1, "problema": 14, "proceso":1 ,  "text":'INJECTION_14', "tags":'Líneas de flujo Flow lines'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":15, "plastico": 1, "problema": 15, "proceso":1 ,  "text":'INJECTION_15', "tags":'Piezas incompletas Incomplete parts'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":16, "plastico": 1, "problema": 16, "proceso":1 ,  "text":'INJECTION_16', "tags":'Líneas de soldadura deficientes Poor welding lines'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":17, "plastico": 1, "problema": 17, "proceso":1 ,  "text":'INJECTION_17', "tags":'No hay carga del material There is no material charge'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":18, "plastico": 1, "problema": 18, "proceso":1 ,  "text":'INJECTION_18', "tags":'Alabeo, pandeo Warping, buckling'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":19, "plastico": 1, "problema": 19, "proceso":1 ,  "text":'INJECTION_19', "tags":'Rayas plata Silver stripes'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":20, "plastico": 1, "problema": 20, "proceso":1 ,  "text":'INJECTION_20', "tags":'Hundimiento y/o huecos (rechupes) en la pieza Sinking and / or holes (rechupes) in the piece'});

    /* MOLDEO POR EXTRUSIÓN DE SOPLADO */

    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":21, "plastico": 1, "problema": 1, "proceso":2 ,  "text":'SOPLADO_1', "tags":'Reventar / Estallado'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":22, "plastico": 1, "problema": 2, "proceso":2 ,  "text":'SOPLADO_2', "tags":'Burbujas'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":23, "plastico": 1, "problema": 3, "proceso":2 ,  "text":'SOPLADO_3', "tags":'Curvatura en el parison'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":24, "plastico": 1, "problema": 4, "proceso":2 ,  "text":'SOPLADO_4', "tags":'En forma de cortina (pliegues)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":25, "plastico": 1, "problema": 5, "proceso":2 ,  "text":'SOPLADO_5', "tags":'Líneas del dado'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":26, "plastico": 1, "problema": 6, "proceso":2 ,  "text":'SOPLADO_6', "tags":'Fractura de fundido (superficie áspera / ondas en el envase terminado, también conocido como piel de tiburón)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":27, "plastico": 1, "problema": 7, "proceso":2 ,  "text":'SOPLADO_7', "tags":'Parison no uniforme (puntos o áreas frías en el parison)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":28, "plastico": 1, "problema": 8, "proceso":2 ,  "text":'SOPLADO_8', "tags":'Fluctuaciones en producción (extrusor)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":29, "plastico": 1, "problema": 9, "proceso":2 ,  "text":'SOPLADO_9', "tags":'Hundimiento o decaído (exceso de holgura o estiramiento)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":30, "plastico": 1, "problema": 10, "proceso":2 ,  "text":'SOPLADO_10', "tags":'Partículas o contaminación (en las paredes del envase)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":31, "plastico": 1, "problema": 11, "proceso":2 ,  "text":'SOPLADO_11', "tags":'Poco hinchamiento'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":32, "plastico": 1, "problema": 12, "proceso":2 ,  "text":'SOPLADO_12', "tags":'Mucho hinchamiento'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":33, "plastico": 1, "problema": 13, "proceso":2 ,  "text":'SOPLADO_13', "tags":'Corte angular / inclinado (diferencia en altura de lado a lado o de enfrente hacia atrás)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":34, "plastico": 1, "problema": 14, "proceso":2 ,  "text":'SOPLADO_14', "tags":'Cuello estrangulado'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":35, "plastico": 1, "problema": 15, "proceso":2 ,  "text":'SOPLADO_15', "tags":'Cuello virado (inclinación en el acabado, dimensión “H” es muy alta en un lado y baja en el otro)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":36, "plastico": 1, "problema": 16, "proceso":2 ,  "text":'SOPLADO_16', "tags":'Depresión (cóncava) terminado (punto deformado, área baja en la superficie de sellado)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":37, "plastico": 1, "problema": 17, "proceso":2 ,  "text":'SOPLADO_17', "tags":'Cuello con rebaba (rebaba en el cuello por un lado del envase)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":38, "plastico": 1, "problema": 18, "proceso":2 ,  "text":'SOPLADO_18', "tags":'Rebaba en la rosca del cuello (dimensiones “T” y “E” solamente)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":39, "plastico": 1, "problema": 19, "proceso":2 ,  "text":'SOPLADO_19', "tags":'Rebaba del cuello no desprendida (completamente)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":40, "plastico": 1, "problema": 20, "proceso":2 ,  "text":'SOPLADO_20', "tags":'Arrugas / mal acabado en la superficie de sellar (cuello)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":41, "plastico": 1, "problema": 21, "proceso":2 ,  "text":'SOPLADO_21', "tags":'Cóncavo / panel hundido (uno o ambos lados donde se etiqueta)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":42, "plastico": 1, "problema": 22, "proceso":2 ,  "text":'SOPLADO_22', "tags":'Definición de detalles (mala definición en los detalles como letras, números o características)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":43, "plastico": 1, "problema": 23, "proceso":2 ,  "text":'SOPLADO_23', "tags":'Paneles del envase abollados o con depresiones'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":44, "plastico": 1, "problema": 24, "proceso":2 ,  "text":'SOPLADO_24', "tags":'Líneas o rayas del dado (líneas verticales o líneas en el interior o exterior del envase que se extienden desde la parte superior a la inferior) '});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":45, "plastico": 1, "problema": 25, "proceso":2 ,  "text":'SOPLADO_25', "tags":'Brillo (satinado u opacado)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":46, "plastico": 1, "problema": 26, "proceso":2 ,  "text":'SOPLADO_26', "tags":'Perforación / ruptura de línea de separación (envases con asa o rebaba extensa)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":47, "plastico": 1, "problema": 27, "proceso":2 ,  "text":'SOPLADO_27', "tags":'Mala penetración de la aguja de soplo (máquina equipada)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":48, "plastico": 1, "problema": 28, "proceso":2 ,  "text":'SOPLADO_28', "tags":'Marca de congelación en panel'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":49, "plastico": 1, "problema": 29, "proceso":2 ,  "text":'SOPLADO_29', "tags":'Apariencia de la superficie (costra, escama, puntos fríos, etc.)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":50, "plastico": 1, "problema": 30, "proceso":2 ,  "text":'SOPLADO_30', "tags":'Fallas en desbarbar / troquelar (con asa y rebaba alrededor del cuello)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":51, "plastico": 1, "problema": 31, "proceso":2 ,  "text":'SOPLADO_31', "tags":'Áreas y/o paredes muy delgadas (incluyendo paneles, circunferencia, por todo el envase, rayas y líneas de separación)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":52, "plastico": 1, "problema": 32, "proceso":2 ,  "text":'SOPLADO_32', "tags":'Pandeo, caída o deformación (del envase)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":53, "plastico": 1, "problema": 33, "proceso":2 ,  "text":'SOPLADO_33', "tags":'Deformación (dentro del envase)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":54, "plastico": 1, "problema": 34, "proceso":2 ,  "text":'SOPLADO_34', "tags":'Base deformada, hinchada (inestable)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":55, "plastico": 1, "problema": 35, "proceso":2 ,  "text":'SOPLADO_35', "tags":'La cola no se desprende (la cola completa o una parte se queda enganchada al envase)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":56, "plastico": 1, "problema": 36, "proceso":2 ,  "text":'SOPLADO_36', "tags":'Rasgando'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":57, "plastico": 1, "problema": 37, "proceso":2 ,  "text":'SOPLADO_37', "tags":'Base con soldadura delgada / débil'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":58, "plastico": 1, "problema": 38, "proceso":2 ,  "text":'SOPLADO_38', "tags":'Bajo ESCR'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":59, "plastico": 1, "problema": 39, "proceso":2 ,  "text":'SOPLADO_39', "tags":'Baja resistencia al impacto'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":60, "plastico": 1, "problema": 40, "proceso":2 ,  "text":'SOPLADO_40', "tags":'Resistencia a carga vertical'});

    /* EXTRUSIÓN DE MONOFILAMENTOS */

    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":61, "plastico": 1, "problema": 1, "proceso":3 ,  "text":'MONOFILAMENTOS_1', "tags":'Rotura de filamento'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":62, "plastico": 1, "problema": 2, "proceso":3 ,  "text":'MONOFILAMENTOS_2', "tags":'Variación del diámetro de los filamentos'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":63, "plastico": 1, "problema": 3, "proceso":3 ,  "text":'MONOFILAMENTOS_3', "tags":'Filamentos con diferente tamaño y/o fuerza'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":64, "plastico": 1, "problema": 4, "proceso":3 ,  "text":'MONOFILAMENTOS_4', "tags":'Mala apariencia de la superficie'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":65, "plastico": 1, "problema": 5, "proceso":3 ,  "text":'MONOFILAMENTOS_5', "tags":'Baja tenacidad'});

    /* TERMOFORMADO */

    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":66, "plastico": 1, "problema": 1, "proceso":4 ,  "text":'TERMOFORMADO_1', "tags":'Burbuja o ampolla en la hoja'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":67, "plastico": 1, "problema": 2, "proceso":4 ,  "text":'TERMOFORMADO_2', "tags":'Detalles y formas incompletas'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":68, "plastico": 1, "problema": 3, "proceso":4 ,  "text":'TERMOFORMADO_3', "tags":'Cambio de color en la hoja'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":69, "plastico": 1, "problema": 4, "proceso":4 ,  "text":'TERMOFORMADO_4', "tags":'Alabeo o pandeo excesivo de la hoja'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":70, "plastico": 1, "problema": 5, "proceso":4 ,  "text":'TERMOFORMADO_5', "tags":'Marcas por enfriamiento en la pieza formada'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":71, "plastico": 1, "problema": 6, "proceso":4 ,  "text":'TERMOFORMADO_6', "tags":'Pequeñas arrugas o marcas circulares'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":72, "plastico": 1, "problema": 7, "proceso":4 ,  "text":'TERMOFORMADO_7', "tags":'Variación en el pandeo de la hoja'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":73, "plastico": 1, "problema": 8, "proceso":4 ,  "text":'TERMOFORMADO_8', "tags":'Arrugas durante el formado'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":74, "plastico": 1, "problema": 9, "proceso":4 ,  "text":'TERMOFORMADO_9', "tags":'Líneas o zonas muy brillantes'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":75, "plastico": 1, "problema": 10, "proceso":4 ,  "text":'TERMOFORMADO_10', "tags":'Mala apariencia de la superficie de la pieza'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":76, "plastico": 1, "problema": 11, "proceso":4 ,  "text":'TERMOFORMADO_11', "tags":'Distorsión excesiva o encogimiento después de desmoldar la pieza'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":77, "plastico": 1, "problema": 12, "proceso":4 ,  "text":'TERMOFORMADO_12', "tags":'Excesivo adelgazamiento del espesor de la pared de la pieza'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":78, "plastico": 1, "problema": 13, "proceso":4 ,  "text":'TERMOFORMADO_13', "tags":'Torcedura de las piezas'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":79, "plastico": 1, "problema": 14, "proceso":4 ,  "text":'TERMOFORMADO_14', "tags":'Marcas de encogimiento en las esquinas'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":80, "plastico": 1, "problema": 15, "proceso":4 ,  "text":'TERMOFORMADO_15', "tags":'Pre-estiramiento de la burbuja no uniforme'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":81, "plastico": 1, "problema": 16, "proceso":4 ,  "text":'TERMOFORMADO_16', "tags":'Esquinas de espesor delgado en formados de profundidad'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":82, "plastico": 1, "problema": 17, "proceso":4 ,  "text":'TERMOFORMADO_17', "tags":'La pieza se amarra al molde'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":83, "plastico": 1, "problema": 18, "proceso":4 ,  "text":'TERMOFORMADO_18', "tags":'Las esquinas de la pieza formada se estrellan una vez en servicio'});
    this.problemas.add(this.problemas_procesos);

    /* EXTRUCCION DE TUBOS */

    this.problemas_procesos = ({ "id":84, "plastico": 1, "problema": 1, "proceso":5 ,  "text":'ETXTUBOS_1', "tags":'Líneas del dado (extruido)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":85, "plastico": 1, "problema": 2, "proceso":5 ,  "text":'ETXTUBOS_2', "tags":'Pulsación del extrusor'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":86, "plastico": 1, "problema": 3, "proceso":5 ,  "text":'ETXTUBOS_3', "tags":'Geles y otros contaminantes en el tubo'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":87, "plastico": 1, "problema": 4, "proceso":5 ,  "text":'ETXTUBOS_4', "tags":'Puntos gruesos en la pared del tubo'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":88, "plastico": 1, "problema": 5, "proceso":5 ,  "text":'ETXTUBOS_5', "tags":'Tubería redondeada (ovalidad)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":89, "plastico": 1, "problema": 6, "proceso":5 ,  "text":'ETXTUBOS_6', "tags":'Rasgado en la tubería'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":90, "plastico": 1, "problema": 7, "proceso":5 ,  "text":'ETXTUBOS_7', "tags":'Hundimiento / decaimiento en la tubería (sagging)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":91, "plastico": 1, "problema": 8, "proceso":5 ,  "text":'ETXTUBOS_8', "tags":'Superficie rugosa (pared interna o externa del tubo)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":92, "plastico": 1, "problema": 9, "proceso":5 ,  "text":'ETXTUBOS_9', "tags":'Degradación térmica del tubo (falla en el tiempo de inducción oxidativa, TIO)'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":93, "plastico": 1, "problema": 10, "proceso":5 ,  "text":'ETXTUBOS_10', "tags":'Corte de tubos desigual'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":94, "plastico": 1, "problema": 11, "proceso":5 ,  "text":'ETXTUBOS_11', "tags":'Grosor de pared del tubo desigual'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":95, "plastico": 1, "problema": 12, "proceso":5 ,  "text":'ETXTUBOS_12', "tags":'Huecos en el tubo'});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":96, "plastico": 1, "problema": 13, "proceso":5 ,  "text":'ETXTUBOS_13', "tags":'Falta de resistencia en la tubería'});
    this.problemas.add(this.problemas_procesos);

    /* EXTRUCCION DE PELICULAS */
    
    this.problemas_procesos = ({ "id":97, "plastico": 1, "problema": 1, "proceso":6 ,  "text":'EXTPEL_1', "tags":''});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":98, "plastico": 1, "problema": 2, "proceso":6 ,  "text":'EXTPEL_2', "tags":''});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":99, "plastico": 1, "problema": 3, "proceso":6 ,  "text":'EXTPEL_3', "tags":''});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":100, "plastico": 1, "problema": 4, "proceso":6 ,  "text":'EXTPEL_4', "tags":''});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":101, "plastico": 1, "problema": 5, "proceso":6 ,  "text":'EXTPEL_5', "tags":''});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":102, "plastico": 1, "problema": 6, "proceso":6 ,  "text":'EXTPEL_6', "tags":''});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":103, "plastico": 1, "problema": 7, "proceso":6 ,  "text":'EXTPEL_7', "tags":''});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":104, "plastico": 1, "problema": 8, "proceso":6 ,  "text":'EXTPEL_8', "tags":''});
    this.problemas.add(this.problemas_procesos);   

      /* COEXTRUCCION DE PELICULAS */
    
    this.problemas_procesos = ({ "id":105, "plastico": 1, "problema": 1, "proceso":7 ,  "text":'COEXTPEL_1', "tags":''});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":106, "plastico": 1, "problema": 2, "proceso":7 ,  "text":'COEXTPEL_2', "tags":''});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":107, "plastico": 1, "problema": 3, "proceso":7 ,  "text":'COEXTPEL_3', "tags":''});
    this.problemas.add(this.problemas_procesos);
    this.problemas_procesos = ({ "id":108, "plastico": 1, "problema": 4, "proceso":7 ,  "text":'COEXTPEL_4', "tags":''});
    this.problemas.add(this.problemas_procesos);
    console.log(this.problemas);
  }

  
  getProblems(ev) {    
    let val = ev.target.value;
    console.log(val);
    if (!val || !val.trim()) {
      this.problems = this.filterProblemas;
      console.log(this.problems);
      return;
    }
    this.problems = this.problemas.query({
      tags: val      
    });
  }
  

  onScroll($event: CustomEvent<ScrollDetail>) {
    if ($event && $event.detail && $event.detail.scrollTop) {
      const scrollTop = $event.detail.scrollTop;
      this.showToolbar = scrollTop >= 225;
    }
  }

// Helper
  async showToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
   
   checkDetail(problem){
     console.log(problem);
      this.router.navigate(['/subdetail', problem]);
   }
}
