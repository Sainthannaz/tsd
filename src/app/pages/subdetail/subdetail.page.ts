import { Component, OnInit, ViewChild, ElementRef, Directive, Input, Renderer2, SimpleChanges} from '@angular/core';
import { MenuController, NavController, Platform, ToastController, LoadingController, IonSlides, DomController, AlertController  } from '@ionic/angular';
import {ActivatedRoute} from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ScrollDetail,  } from '@ionic/core';
import { Router } from  "@angular/router";
import { Causas } from "../../mocks/providers/causas";
import { Causa } from "../../models/causa";
import { Soluciones } from "../../mocks/providers/soluciones";
import { Solucion } from "../../models/solucion";

@Component({
  selector: 'app-subdetail',
  templateUrl: './subdetail.page.html',
  styleUrls: ['./subdetail.page.scss'],
})


export class SubdetailPage implements OnInit {
problem :  any = [];
causas_procesos: any = [];
soluciones_procesos: any = [];
mostrarCausas : any;
mostrarSoluciones : any;

constructor(public navCtrl: NavController,  private route: ActivatedRoute, public platform: Platform, public loadingCtrl: LoadingController, private translate: TranslateService,
    public menu: MenuController, private  router:  Router, public toastCtrl: ToastController, public alertCtrl: AlertController, 
    public causas: Causas, public soluciones: Soluciones) {
  	this.problem.problema = this.route.snapshot.paramMap.get('problema');    
    this.problem.plastico = this.route.snapshot.paramMap.get('plastico');
    this.problem.text = this.route.snapshot.paramMap.get('text');
    console.log(this.problem);
    this.mostrarCausas = this.causas.query();
    this.mostrarSoluciones = this.soluciones.query();
    this.loadDataCausa();
    this.loadDataSolucion();
  }

  ngOnInit() {
  	
  }

  loadDataCausa(){
    this.causas.deleteAll();
    this.causas_procesos = ({ "problema": 1, "proceso":1, "causa": 1, "text":'CAUSAS_1' });
    this.causas.add(this.causas_procesos);
    this.causas_procesos = ({ "problema": 1,"proceso":1 ,  "causa": 2, "text":'CAUSAS_2' });
    this.causas.add(this.causas_procesos);
    this.causas_procesos = ({ "problema": 1,"proceso":1 ,  "causa": 3, "text":'CAUSAS_3' });
    this.causas.add(this.causas_procesos);
    this.causas_procesos = ({ "problema": 2,"proceso":1 ,  "causa": 1, "text":'CAUSAS_4' });
    this.causas.add(this.causas_procesos);
    this.causas_procesos = ({ "problema": 2,"proceso":1 ,  "causa": 2, "text":'CAUSAS_5' });
    this.causas.add(this.causas_procesos);
    this.causas_procesos = ({ "problema": 2,"proceso":1 ,  "causa": 3, "text":'CAUSAS_6' });
    this.causas.add(this.causas_procesos);
    this.causas_procesos = ({ "problema": 2,"proceso":1 ,  "causa": 4, "text":'CAUSAS_7' });
    this.causas.add(this.causas_procesos);
    this.causas_procesos = ({ "problema": 3,"proceso":1 ,  "causa": 1, "text":'CAUSAS_8' });
    this.causas.add(this.causas_procesos);
    this.causas_procesos = ({ "problema": 3,"proceso":1 ,  "causa": 2, "text":'CAUSAS_9' });
    this.causas.add(this.causas_procesos);
    this.causas_procesos = ({ "problema": 3,"proceso":1 ,  "causa": 3, "text":'CAUSAS_10' });
    this.causas.add(this.causas_procesos);
    this.causas_procesos = ({ "problema": 4,"proceso":1 ,  "causa": 1, "text":'CAUSAS_11' });
    this.causas.add(this.causas_procesos);
    this.causas_procesos = ({ "problema": 5,"proceso":1 ,  "causa": 1, "text":'CAUSAS_12' });
    this.causas.add(this.causas_procesos);
    this.causas_procesos = ({ "problema": 5,"proceso":1 ,  "causa": 2, "text":'CAUSAS_13' });
    this.causas.add(this.causas_procesos);
    this.causas_procesos = ({ "problema": 5,"proceso":1 ,  "causa": 3, "text":'CAUSAS_14' });
    this.causas.add(this.causas_procesos);
    this.causas_procesos = ({ "problema": 5,"proceso":1 ,  "causa": 4, "text":'CAUSAS_15' });
    this.causas.add(this.causas_procesos);
  }

  loadDataSolucion(){
    this.soluciones.deleteAll();
    this.soluciones_procesos = ({ "causa": 1, "proceso":1 , "solucion": 1, "text":'SOLUCION_1' });
    this.soluciones.add(this.soluciones_procesos);
    this.soluciones_procesos = ({ "causa": 1,"proceso":1 ,  "solucion": 2, "text":'SOLUCION_2' });
    this.soluciones.add(this.soluciones_procesos);
    this.soluciones_procesos = ({ "causa": 1,"proceso":1 ,  "solucion": 3, "text":'SOLUCION_3' });
    this.soluciones.add(this.soluciones_procesos);
    this.soluciones_procesos = ({ "causa": 1,"proceso":1 ,  "solucion": 4, "text":'SOLUCION_4' });
    this.soluciones.add(this.soluciones_procesos);
    this.soluciones_procesos = ({ "causa": 1,"proceso":1 ,  "solucion": 5, "text":'SOLUCION_5' });
    this.soluciones.add(this.soluciones_procesos);
    this.soluciones_procesos = ({ "causa": 2,"proceso":1 ,  "solucion": 1, "text":'SOLUCION_6' });
    this.soluciones.add(this.soluciones_procesos);
    this.soluciones_procesos = ({ "causa": 2,"proceso":1 ,  "solucion": 2, "text":'SOLUCION_7' });
    this.soluciones.add(this.soluciones_procesos);
    this.soluciones_procesos = ({ "causa": 2,"proceso":1 ,  "solucion": 3, "text":'SOLUCION_8' });
    this.soluciones.add(this.soluciones_procesos);
    this.soluciones_procesos = ({ "causa": 3,"proceso":1 ,  "solucion": 1, "text":'SOLUCION_9' });
    this.soluciones.add(this.soluciones_procesos);
    this.soluciones_procesos = ({ "causa": 3,"proceso":1 ,  "solucion": 2, "text":'SOLUCION_10' });
    this.soluciones.add(this.soluciones_procesos);
    this.soluciones_procesos = ({ "causa": 4,"proceso":1 ,  "solucion": 1, "text":'SOLUCION_11' });
    this.soluciones.add(this.soluciones_procesos);
    this.soluciones_procesos = ({ "causa": 4,"proceso":1 ,  "solucion": 2, "text":'SOLUCION_12' });
    this.soluciones.add(this.soluciones_procesos);
    this.soluciones_procesos = ({ "causa": 4,"proceso":1 ,  "solucion": 3, "text":'SOLUCION_13' });
    this.soluciones.add(this.soluciones_procesos);
    this.soluciones_procesos = ({ "causa": 4,"proceso":1 ,  "solucion": 4, "text":'SOLUCION_14' });
    this.soluciones.add(this.soluciones_procesos);
  }

}