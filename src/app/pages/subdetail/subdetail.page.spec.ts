import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubdetailPage } from './subdetail.page';

describe('SubdetailPage', () => {
  let component: SubdetailPage;
  let fixture: ComponentFixture<SubdetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubdetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubdetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
