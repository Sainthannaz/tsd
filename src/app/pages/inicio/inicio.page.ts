import { Component, OnInit } from '@angular/core';
import { MenuController, NavController, Platform, ToastController, LoadingController } from '@ionic/angular';
import { Router } from  "@angular/router";
import { Storage } from '@ionic/storage';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';
  user = { email:'', password:''}
  validUser: any = [];
  //validUser: { user:string, password:string}; 
  public unregisterBackButtonAction: any;
  private myToast: any;
  constructor(public navCtrl: NavController,  public platform: Platform, public loadingCtrl: LoadingController, public menu: MenuController, 
    private  router:  Router, private storage: Storage, private authenticationService: AuthenticationService, public toastController: ToastController) {

  }
  
  ngOnInit() {
    this.menu.enable(false);
    this.validUser.push({user:"TS&D1",password:"Techserv1"});
    this.validUser.push({user:"TS&D2",password:"Techserv2"});
    this.validUser.push({user:"TS&D3",password:"Techserv3"});
    this.validUser.push({user:"TS&D4",password:"Techserv4"});
    this.validUser.push({user:"TS&D5",password:"Techserv5"});
    this.validUser.push({user:"TS&D6",password:"Techserv6"});
    this.validUser.push({user:"TS&D7",password:"Techserv7"});
    this.validUser.push({user:"TS&D8",password:"Techserv8"});
    this.validUser.push({user:"TS&D9",password:"Techserv9"});
    this.validUser.push({user:"TS&D10",password:"Techserv10"});
    this.validUser.push({user:"TS&D11",password:"Techserv11"});
    this.validUser.push({user:"TS&D12",password:"Techserv12"});
    this.validUser.push({user:"TS&D13",password:"Techserv13"});
    this.validUser.push({user:"TS&D14",password:"Techserv14"});
    this.validUser.push({user:"TS&D15",password:"Techserv15"});
    console.log(this.validUser);

    this.initializeBackButtonCustomHandler();
      this.storage.get('tutorial').then((result) => {
        console.log("El tutorial ya se vio!");
        console.log(result);
        if(result){
          this.storage.get('loginSession').then((val) => {
            console.log('El estado de la sesión es: ', val);
            if (val == "true"){
              console.log("true");
              // En caso de que ya se vio el estado de la sesion de manera local, se rectifica mediante el estado del Guard
              this.authenticationService.authenticationState.subscribe(state => {
                if (state) {
                  console.log(state);
                  this.router.navigateByUrl('dashboard');
                } else {
                  this.authenticationService.login(); // Guard para inicio de sesión
                  this.storage.set('loginSession', 'true');
                  console.log(state);
                  this.router.navigateByUrl('dashboard');
                  //this.router.navigateByUrl('login');
                }
              });            
            }

            if (val == "false"){
              console.log("false");
            }
          });
        } else {
          this.router.navigateByUrl('tutorial');        
        }
        
      });
      return;
  }

  goToDashboard(){
    this.router.navigateByUrl('dashboard');
  }

  checkLogin(){
    this.validUser.forEach( (item, index) => {   
      //console.log(item.user);
      if (this.user.email == item.user && this.user.password == item.password)
      {
        this.authenticationService.login(); // Guard para inicio de sesión
        this.router.navigateByUrl('dashboard'); // Redirecciona posterior al Guard
      } else {
        //this.showToast("Usuario incorrecto");
      }
    });

    
  }

  hideShowPassword() {
     this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
     this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.backButton.subscribe(() => {
    // Interceptamos el presionar el botón atras
  })
  }

   ionViewWillLeave() {   
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();   
  }

  showToast(message) {
    this.myToast = this.toastController.create({
      message: message,
      duration: 2000
    }).then((toastData) => {
      console.log(toastData);
      toastData.present();
    });
  }
}
