
import { Injectable } from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {ToastController} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  _user: any;


  constructor(private storage: Storage, public toastCtrl: ToastController, public translateService: TranslateService) { }

  login(accountInfo: any) {
 
  }

  logout(userdata: any) {
 
  }

  signup(accountInfo: any) {
    
  }
  
  _loggedIn(resp) {
    this._user = resp.user;
  }
}
