import { Injectable } from '@angular/core';

import { Solucion } from '../../models/solucion';

@Injectable()
export class Soluciones {
  soluciones: Solucion[] = [];

  defaultSolucion: any = {

  };


  constructor() {
    let soluciones = [

    ];

    for (let solucion of soluciones) {
      this.soluciones.push(new Solucion(solucion));
    }
  }

  query(params?: any) {
    if (!params) {
      return this.soluciones;
    }

    return this.soluciones.filter((solucion) => {
      for (let key in params) {
        let field = solucion[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return solucion;
        } else if (field == params[key]) {
          return solucion;
        }
      }
      return null;
    });
  }

  add(solucion: Solucion) {
    this.soluciones.push(solucion);
  }

  delete(solucion: Solucion) {
    this.soluciones.splice(this.soluciones.indexOf(solucion), 1);
  }

  deleteAll() {
    this.soluciones.splice(0);
  }
}
