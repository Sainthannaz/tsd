import { Injectable } from '@angular/core';

import { Causa } from '../../models/causa';

@Injectable()
export class Causas {
  causas: Causa[] = [];

  defaultCausa: any = {

  };


  constructor() {
    let causas = [

    ];

    for (let causa of causas) {
      this.causas.push(new Causa(causa));
    }
  }

  query(params?: any) {
    if (!params) {
      return this.causas;
    }

    return this.causas.filter((causa) => {
      for (let key in params) {
        let field = causa[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return causa;
        } else if (field == params[key]) {
          return causa;
        }
      }
      return null;
    });
  }

  add(causa: Causa) {
    this.causas.push(causa);
  }

  delete(causa: Causa) {
    this.causas.splice(this.causas.indexOf(causa), 1);
  }

  deleteAll() {
    this.causas.splice(0);
  }
}
