import { Injectable } from '@angular/core';
import { Problem } from '../../models/problem';


@Injectable()
export class Problems {
  problems: Problem[] = [];

  defaultProblem: any = {

  };


  constructor() {
    let problems = [

    ];

    for (let problem of problems) {
      this.problems.push(new Problem(problem));
    }
  }


  query(params?: any) {
    if (!params) {
      return this.problems;
    }

    return this.problems.filter((problem) => {
      console.log(problem);
      for (let key in params) {
        let field = problem[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return problem;
        } else if (field == params[key]) {
          return problem;
        }
      }
      return null;
    });
  }

  getAll(){
    return this.problems;
  }

  add(problem: Problem) {
    this.problems.push(problem);
  }

  delete(problem: Problem) {
    this.problems.splice(this.problems.indexOf(problem), 1);
  }

  deleteAll() {
    this.problems.splice(0);
  }
}
