import { Injectable } from '@angular/core';

import { Plastico } from '../../models/plastico';

@Injectable()
export class Plasticos {
  plasticos: Plastico[] = [];

  defaultPlastico: any = {

  };


  constructor() {
    let plasticos = [

    ];

    for (let plastico of plasticos) {
      this.plasticos.push(new Plastico(plastico));
    }
  }

  query(params?: any) {
    if (!params) {
      return this.plasticos;
    }

    return this.plasticos.filter((plastico) => {
      for (let key in params) {
        let field = plastico[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return plastico;
        } else if (field == params[key]) {
          return plastico;
        }
      }
      return null;
    });
  }

  add(plastico: Plastico) {
    this.plasticos.push(plastico);
  }

  delete(plastico: Plastico) {
    this.plasticos.splice(this.plasticos.indexOf(plastico), 1);
  }

  deleteAll() {
    this.plasticos.splice(0);
  }
}
