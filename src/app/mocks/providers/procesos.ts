import { Injectable } from '@angular/core';

import { Proceso } from '../../models/proceso';

@Injectable()
export class Procesos {
  procesos: Proceso[] = [];

  defaultProceso: any = {

  };


  constructor() {
    let procesos = [

    ];

    for (let proceso of procesos) {
      this.procesos.push(new Proceso(proceso));
    }
  }

  query(params?: any) {
    if (!params) {
      return this.procesos;
    }

    return this.procesos.filter((proceso) => {
      for (let key in params) {
        let field = proceso[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return proceso;
        } else if (field == params[key]) {
          return proceso;
        }
      }
      return null;
    });
  }

  add(proceso: Proceso) {
    this.procesos.push(proceso);
  }

  delete(proceso: Proceso) {
    this.procesos.splice(this.procesos.indexOf(proceso), 1);
  }

  deleteAll() {
    this.procesos.splice(0);
  }
}
