import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'inicio',
    pathMatch: 'full'
  },  
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },    
  { path: 'inicio', loadChildren: './pages/inicio/inicio.module#InicioPageModule' },
  { path: 'dashboard', loadChildren: './pages/dashboard/dashboard.module#DashboardPageModule'}, 
  { path: 'tutorial', loadChildren: './pages/tutorial/tutorial.module#TutorialPageModule' },
  { path: 'detail', loadChildren: './pages/detail/detail.module#DetailPageModule' },      
  { path: 'about', loadChildren: './pages/about/about.module#AboutPageModule'},  
  //{ path: 'card', loadChildren: './modals/card/card.module#CardPageModule', canActivate: [AuthGuard] },
  { path: 'subdetail', loadChildren: './pages/subdetail/subdetail.module#SubdetailPageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' }


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
